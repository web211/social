package dhbw.web.social;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import dhbw.web.interfaces.service.AnswerService;
import dhbw.web.interfaces.service.CommentService;
import dhbw.web.interfaces.service.QuestionService;
import dhbw.web.interfaces.service.UserService;
import dhbw.web.model.db.mapper.ModelDBMapper;
import dhbw.web.model.db.repository.FollowDBRepository;
import dhbw.web.model.db.repository.LikeDBRepository;
import dhbw.web.model.db.repository.NotificationDBRepository;
import dhbw.web.service.AnswerServiceImpl;
import dhbw.web.service.CommentServiceImpl;
import dhbw.web.service.QuestionServiceImpl;
import dhbw.web.service.UserServiceImpl;
import dhbw.web.utils.MapperWrapper;

@EnableDiscoveryClient
@SpringBootApplication(scanBasePackages = { "dhbw.web.social", "dhbw.web.service", "dhbw.web.model.db.mapper" })
public class SocialApplication {

	public static void main(String[] args) {
		SpringApplication.run(SocialApplication.class, args);
	}

	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public QuestionService questionService() {
		return new QuestionServiceImpl();
	}

	@Bean
	public CommentService commentService() {
		return new CommentServiceImpl();
	}

	@Bean
	public UserService userService() {
		return new UserServiceImpl();
	}

	@Bean
	public AnswerService answerService() {
		return new AnswerServiceImpl();
	}

	@Bean
	public MapperWrapper mapper() {
		return ModelDBMapper.getModelDBMapper();
	}

	@Bean
	public FollowDBRepository followRepo() {
		return new FollowDBRepository();
	}

	@Bean
	public LikeDBRepository likeRepo() {
		return new LikeDBRepository();
	}

	@Bean
	public NotificationDBRepository notificationRepo() {
		return new NotificationDBRepository();
	}

}
