package dhbw.web.social;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.ElementParent;
import dhbw.web.interfaces.ModelFactory;
import dhbw.web.interfaces.service.CommentService;
import dhbw.web.interfaces.service.NotificationService;
import dhbw.web.interfaces.service.QuestionService;
import dhbw.web.interfaces.service.UserService;
import dhbw.web.model.Answer;
import dhbw.web.model.Comment;
import dhbw.web.model.Follow;
import dhbw.web.model.Notification;
import dhbw.web.model.Question;
import dhbw.web.model.User;
import dhbw.web.model.db.NotificationDB;
import dhbw.web.model.db.repository.NotificationDBRepository;
import dhbw.web.model.form.NotificationType;
import dhbw.web.model.form.PushNotificationForm;
import dhbw.web.utils.MapperWrapper;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/notify")
@Slf4j
public class NotifyController implements NotificationService {

	@Autowired
	private QuestionService questionService;

	@Autowired
	private UserService userService;

	@Autowired
	private CommentService commentService;

	@Autowired
	private FollowController followService;

	@Autowired
	private MapperWrapper dbMapper;

	@Autowired
	private NotificationDBRepository notificationDBRepository;

	@LoadBalanced
	@PostMapping("")
	@Override
	public Notification[] publish(@RequestBody PushNotificationForm form) {

		log.debug(String.format("publish %s, of parent with id %d and type %s. the user with id %d is responsible",
				form.getType(), form.getParent().getId(), form.getParent().getType(), form.getUserId()));
		List<Notification> notifys = ModelFactory.getFactory().getList();
		Follow[] followerUser = this.followService.readFollow(form.getUserId(), User.TYPE, 0);
		User u = form.getUser() == null ? this.userService.readUser(form.getUserId()) : form.getUser();
		switch (form.getParent().getType()) {
		case Answer.TYPE:
			handleAnswer(form, followerUser, notifys, u);
		case Comment.TYPE:
			handleComment(form, followerUser, notifys, u);
		case Question.TYPE:
			handleaQuestion(form, followerUser, notifys, u);
		}

		for (Notification notify : notifys) {
			notify.setId(notificationDBRepository.save(dbMapper.map(notify, NotificationDB.class)));
		}

		log.debug(String.format("%d notifications got created", notifys.size()));

		return notifys.toArray(new Notification[notifys.size()]);

	}

	@LoadBalanced
	@GetMapping("/{userId}/latest")
	@Override
	public Notification[] getLatestUserNotifications(@PathVariable int userId, @RequestParam int amount) {
		log.debug(String.format("read latest notifications from user %d, amount: %d", userId, amount));

		List<Notification> n = new ArrayList<>();
		Follow[] follows = this.followService.readAllUserFollows(userId);
		for (NotificationDB notification : notificationDBRepository.readLatest(amount, getInts(follows))) {
			Notification notify = dbMapper.map(notification, Notification.class);
			notify.setFollow(getFollowById(follows, notify.getFollow().getId()));
			n.add(notify);
		}
		return n.toArray(new Notification[n.size()]);
	}

	@LoadBalanced
	@GetMapping("/{userId}")
	@Override
	public Notification[] getAllUserNotifications(@PathVariable int userId) {
		// read all notifications
		log.debug("read all notifications from user: " + userId);
		List<Notification> n = new ArrayList<>();
		Follow[] follows = this.followService.readAllUserFollows(userId);
		for (NotificationDB notification : notificationDBRepository.readLatestAll(readFollowIds(userId))) {
			Notification notify = dbMapper.map(notification, Notification.class);
			notify.setFollow(getFollowById(follows, notify.getFollow().getId()));
			n.add(notify);
		}
		return n.toArray(new Notification[n.size()]);
	}

	private Follow getFollowById(Follow[] array, int id) {
		for (Follow follow : array) {
			if (follow.getId() == id) {
				return follow;
			}
		}
		return null;
	}

	private List<Integer> getInts(Follow[] array) {
		return Arrays.asList(array).stream().map(x -> x.getId()).collect(Collectors.toList());
	}

	private List<Integer> readFollowIds(int userId) {
		return Arrays.asList(this.followService.readAllUserFollows(userId)).stream().map(x -> x.getId())
				.collect(Collectors.toList());
	}

	private void handleAnswer(PushNotificationForm form, Follow[] followerUser, List<Notification> notifys, User u) {
		Question aq = this.questionService.readQuestion(form.getParent().getId());
		Follow[] followerQuestion = form.getType() == NotificationType.CREATE ? new Follow[0]
				: this.followService.readFollow(aq.getId(), aq.getType(), 0);

		removeIfExists(followerQuestion, followerUser);

		createNotifications(form, notifys, followerQuestion, form.getParent(),
				"Answer was %s for question with title: " + aq.getTitle());
		createNotifications(form, notifys, followerUser, u,
				"User " + u.getUsername() + " %s answer for question with title: " + aq.getTitle());

	}

	private void handleaQuestion(PushNotificationForm form, Follow[] followerUser, List<Notification> notifys, User u) {
		Follow[] followerQuestion = form.getType() == NotificationType.CREATE ? new Follow[0]
				: this.followService.readFollow(form.getParent().getId(), form.getParent().getType(), 0);

		Question q = this.questionService.readQuestion(form.getParent().getId());

		removeIfExists(followerQuestion, followerUser);

		createNotifications(form, notifys, followerQuestion, form.getParent(),
				"Question was %s with title: " + q.getTitle());
		createNotifications(form, notifys, followerUser, u,
				"User " + u.getUsername() + " %s question with title: " + q.getTitle());

	}

	private void handleComment(PushNotificationForm form, Follow[] followerUser, List<Notification> notifys, User u) {

		Comment c = this.commentService.readComment(form.getParent().getId());

		if (c.getParent().getType() == User.TYPE) {
			handleUserComment(form, followerUser, notifys, u, c);
		} else if (c.getParent().getType() == Answer.TYPE) {
			handleAnswerComment(form, followerUser, notifys, u, c);
		} else {
			handleQuestionComment(form, followerUser, notifys, u, c);
		}
	}

	private void handleUserComment(PushNotificationForm form, Follow[] followerUser, List<Notification> notifys, User u,
			Comment c) {
		User profile = (User) c.getParent();
		Follow[] followerProfile = form.getType() == NotificationType.CREATE ? new Follow[0]
				: this.followService.readFollow(profile.getId(), profile.getType(), 0);
		removeIfExists(followerProfile, followerUser);

		createNotifications(form, notifys, followerProfile, form.getParent(),
				"Comment was %s of user " + profile.getUsername());
		createNotifications(form, notifys, followerUser, u,
				"User " + u.getUsername() + " %s comment for user" + profile.getUsername());
	}

	private void handleAnswerComment(PushNotificationForm form, Follow[] followerUser, List<Notification> notifys,
			User u, Comment c) {
		Answer answer = (Answer) c.getParent();
		Question question = this.questionService.readQuestion(answer.getParentQuestion().getId());
		Follow[] followerQuestion = form.getType() == NotificationType.CREATE ? new Follow[0]
				: this.followService.readFollow(question.getId(), question.getType(), 0);

		removeIfExists(followerQuestion, followerUser);

		createNotifications(form, notifys, followerQuestion, form.getParent(),
				"Comment was %s answer of question with title: " + question.getTitle());
		createNotifications(form, notifys, followerUser, u,
				"User " + u.getUsername() + " %s comment for answer of question with title: " + question.getTitle());
	}

	private void handleQuestionComment(PushNotificationForm form, Follow[] followerUser, List<Notification> notifys,
			User u, Comment c) {
		Question question = (Question) c.getParent();
		Follow[] followerQuestion = form.getType() == NotificationType.CREATE ? new Follow[0]
				: this.followService.readFollow(question.getId(), question.getType(), 0);

		removeIfExists(followerQuestion, followerUser);

		createNotifications(form, notifys, followerQuestion, form.getParent(),
				"Comment was %s for question with title: " + question.getTitle());
		createNotifications(form, notifys, followerUser, u,
				"User " + u.getUsername() + " %s comment for question with title: " + question.getTitle());
	}

	private void removeIfExists(Follow[] followElement, Follow[] followUser) {
		for (int i = 0; i < followElement.length; i++) {
			if (containsFollow(followUser, followElement[i])) {
				followElement[i] = null;
			}
		}
	}

	private boolean containsFollow(Follow[] followUser, Follow follow) {
		for (Follow user : followUser) {
			if (user.getId() == follow.getId()) {
				return true;
			}
		}
		return false;
	}

	private void createNotifications(PushNotificationForm form, List<Notification> notifys, Follow[] follower,
			ElementParent parent, String format) {
		for (Follow follow : follower) {
			if (follow == null) {
				continue;
			}
			Notification n = new Notification();
			n.setCreated(form.getCreated());
			n.setFollow(follow);
			n.setText(String.format(format, getNotificationTypeString(form.getType())));
			notifys.add(n);
		}
	}

	private String getNotificationTypeString(NotificationType type) {
		if (type == NotificationType.CREATE) {
			return "created";
		}
		return "updated";
	}

}
