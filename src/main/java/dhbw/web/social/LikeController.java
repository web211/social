package dhbw.web.social;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.service.LikeService;
import dhbw.web.model.Like;
import dhbw.web.model.db.LikeDB;
import dhbw.web.model.db.repository.LikeDBRepository;
import dhbw.web.model.form.ToggleLikeForm;
import dhbw.web.utils.MapperWrapper;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/like")
@Slf4j
public class LikeController implements LikeService {

	@Autowired
	private MapperWrapper dbMapper;

	@Autowired
	private LikeDBRepository likeDBRepository;

	@Override
	@LoadBalanced
	@PostMapping("")
	public Like toggleLike(@RequestBody ToggleLikeForm form) {
		// read follow from DB
		// if it exists, delete it else create it
		// return empty like if delete, otherwise the newly created object

		log.debug(String.format("toggle like from user %d for element %d of type %s", form.getLiked().getId(),
				form.getElement().getId(), form.getElement().getType()));

		LikeDB l = likeDBRepository.findByUserAndElement(form.getLiked().getId(), form.getElement().getId(),
				form.getElement().getType());

		Like like = new Like();
		if (l == null) {
			like = new Like(form.getLiked(), form.getElement());
			like.setId(likeDBRepository.save(dbMapper.map(like, LikeDB.class)));
		} else {
			likeDBRepository.deleteById(l.getId());
		}
		return like;
	}

	@Override
	@LoadBalanced
	@GetMapping("/{type}/{element}")
	public Like[] readLike(@PathVariable int element, @PathVariable String type,
			@RequestParam(required = false) int userId) {
		// if user is given, read only the user like
		// if user not given, read all likes of the element
		log.debug(String.format("read likes from element %d of type %s", element, type));
		log.debug(String.format("read like from element %d of type %s for user %d", element, type, userId));
		if (userId == 0) {
			List<Like> like = new ArrayList<>();
			for (LikeDB likeDB : likeDBRepository.findByElement(element, type)) {
				like.add(dbMapper.map(likeDB, Like.class));
			}
			return like.toArray(new Like[like.size()]);
		}
		return new Like[] { dbMapper.map(likeDBRepository.findByUserAndElement(userId, element, type), Like.class) };
	}

	@Override
	@LoadBalanced
	@GetMapping("")
	public Like[] readAllLikes() {
		// read all likes from db
		log.debug("read all likes");
		List<Like> like = new ArrayList<>();
		for (LikeDB likeDB : likeDBRepository.findAll()) {
			like.add(dbMapper.map(likeDB, Like.class));
		}
		return like.toArray(new Like[like.size()]);
	}

}
