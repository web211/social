package dhbw.web.social;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dhbw.web.interfaces.service.FollowService;
import dhbw.web.model.Follow;
import dhbw.web.model.db.FollowDB;
import dhbw.web.model.db.repository.FollowDBRepository;
import dhbw.web.model.form.ToggleFollowForm;
import dhbw.web.utils.MapperWrapper;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping(path = "/follow")
@Slf4j
public class FollowController implements FollowService {

	@Autowired
	private FollowDBRepository followDBRepository;

	@Autowired
	private MapperWrapper dbMapper;

	@Override
	@LoadBalanced
	@PostMapping("")
	public Follow toggleFollow(@RequestBody ToggleFollowForm form) {
		// read follow from DB
		// if it exists, delete it else create it
		// return empty like if delete, otherwise the newly created object

		log.debug(String.format("toggle follow from user %d for element %d of type %s", form.getFollower().getId(),
				form.getElement().getId(), form.getElement().getType()));
		FollowDB f = followDBRepository.findByUserAndElement(form.getFollower().getId(), form.getElement().getId(),
				form.getElement().getType());

		Follow follow = new Follow();
		if (f == null) {
			follow = new Follow(form.getFollower(), form.getElement());
			follow.setId(followDBRepository.save(dbMapper.map(follow, FollowDB.class)));
		} else {
			followDBRepository.deleteById(f.getId());
		}
		return follow;
	}

	@Override
	@LoadBalanced
	@GetMapping("/{type}/{element}")
	public Follow[] readFollow(@PathVariable int element, @PathVariable String type, @RequestParam int userId) {
		// if user is given, read only the user follow
		// if user not given, read all follows of the element
		log.debug(String.format("read follows from element %d of type %s", element, type));
		log.debug(String.format("read follow from element %d of type %s for user %d", element, type, userId));
		if (userId == 0) {
			List<Follow> follow = new ArrayList<>();
			for (FollowDB followDB : followDBRepository.findByElement(element, type)) {
				follow.add(dbMapper.map(followDB, Follow.class));
			}
			return follow.toArray(new Follow[follow.size()]);
		}
		return new Follow[] {
				dbMapper.map(followDBRepository.findByUserAndElement(userId, element, type), Follow.class) };
	}

	public Follow[] readAllUserFollows(int userId) {
		log.debug("read all user follows");
		List<Follow> follow = new ArrayList<>();
		for (FollowDB followDB : followDBRepository.findAllUserFollows(userId)) {
			follow.add(dbMapper.map(followDB, Follow.class));
		}
		return follow.toArray(new Follow[follow.size()]);
	}

	@Override
	@LoadBalanced
	@GetMapping("")
	public Follow[] readAllFollower() {
		// read all likes from db
		log.debug("read all follows");
		List<Follow> follow = new ArrayList<>();
		for (FollowDB followDB : followDBRepository.findAll()) {
			follow.add(dbMapper.map(followDB, Follow.class));
		}
		return follow.toArray(new Follow[follow.size()]);
	}

}
